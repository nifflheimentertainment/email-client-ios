//
//  ECMainMenuTableViewController.h
//  EmailClient
//
//  Created by Camilo Rodríguez Gaviria on 22/06/14.
//  Copyright (c) 2014 Nifflheim Entertainment. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ECMainMenuTableViewController : UITableViewController

@end
