//
//  ECSessionManager.m
//  EmailClient
//
//  Created by Camilo Rodríguez Gaviria on 21/06/14.
//  Copyright (c) 2014 Nifflheim Entertainment. All rights reserved.
//

#import "ECSessionManager.h"

@implementation ECSessionManager

#pragma mark - Class Methods

+ (instancetype)sessionManager {
    static dispatch_once_t onceToken;
    static ECSessionManager *manager = nil;
    dispatch_once(&onceToken, ^{
        manager = [[ECSessionManager alloc] init];
    });
    
    return manager;
}

+ (void)updateUsername:(NSString *)username password:(NSString *)password {
    ECSessionManager *sessionManager = [ECSessionManager sessionManager];
    [sessionManager updateUsername:username];
    [sessionManager updatePassword:password];
}

#pragma mark - Initialization

- (id)init {
    self = [super init];
    
    if (self) {
        MCOIMAPSession *session = [[MCOIMAPSession alloc] init];
        session.hostname = @"imap.gmail.com";
        session.port = 993;
        session.connectionType = MCOConnectionTypeTLS;
        self.session = session;
    }
    
    return self;
}

#pragma mark - Public

- (NSString *)username {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults objectForKey:@"username"];
    
    return username;
}

- (NSString *)password {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *password = [defaults objectForKey:@"password"];
    
    return password;
}

- (void)updateUsername:(NSString *)username {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:username forKey:@"username"];
    [defaults synchronize];
    
    self.session.username = username;
}

- (void)updatePassword:(NSString *)password {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:password forKey:@"password"];
    [defaults synchronize];
    
    self.session.password = password;
}

- (void)clear {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"username"];
    [defaults removeObjectForKey:@"password"];
    [self.session cancelAllOperations];
}

@end
