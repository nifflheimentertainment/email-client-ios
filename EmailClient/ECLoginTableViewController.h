//
//  ECLoginTableViewController.h
//  EmailClient
//
//  Created by Camilo Rodríguez Gaviria on 20/06/14.
//  Copyright (c) 2014 Nifflheim Entertainment. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ECLoginTableViewController : UITableViewController

- (IBAction)unwindToLogin:(UIStoryboardSegue *)sender;

@end
