//
//  ECMainMenuTableViewController.m
//  EmailClient
//
//  Created by Camilo Rodríguez Gaviria on 22/06/14.
//  Copyright (c) 2014 Nifflheim Entertainment. All rights reserved.
//

#import "ECMainMenuTableViewController.h"
#import "ECFoldersViewController.h"
#import "ECEmailsTableViewController.h"

NSString *const ECMainMenuSegueFolders = @"Folders";
NSString *const ECMainMenuSegueEmails = @"Emails";
NSString *const ECMainMeneuSegueLogOut = @"LogOut";

@interface ECMainMenuTableViewController () <UITableViewDelegate>

- (IBAction)logout:(UIBarButtonItem *)sender;

@end

@implementation ECMainMenuTableViewController

#pragma mark - Initialization

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
}

#pragma mark - Private

- (IBAction)logout:(UIBarButtonItem *)sender {
    [[ECSessionManager sessionManager] clear];
    
    [self performSegueWithIdentifier:ECMainMeneuSegueLogOut sender:sender];
}

#pragma mark - Overridden

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:ECMainMenuSegueEmails]) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:(UITableViewCell *)sender];
        NSString *folderName = @"";
        NSInteger row = indexPath.row;
        
        if (row == 0) {
            folderName = @"Archive";
        } else if (row == 1) {
            folderName = @"Delete";
        } else if (row == 2) {
            folderName = @"Short Reply";
        } else if (row == 3) {
            folderName = @"Long Reply";
        } else if (row == 4) {
            folderName = @"To Read";
        }
        
        folderName = [NSString stringWithFormat:@"ECFolder/%@", folderName];
        
        ((ECEmailsTableViewController *)segue.destinationViewController).folderName = folderName;
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (indexPath.section == 0 ) {
        [self performSegueWithIdentifier:ECMainMenuSegueEmails sender:cell];
    } else {
        [self performSegueWithIdentifier:ECMainMenuSegueFolders sender:cell];
    }
}

@end
