//
//  ECAppDelegate.m
//  EmailClient
//
//  Created by Camilo Rodríguez Gaviria on 06/05/14.
//  Copyright (c) 2014 Nifflheim Entertainment. All rights reserved.
//

#import <Crashlytics/Crashlytics.h>
#import "ECAppDelegate.h"

@implementation ECAppDelegate

#pragma mark - Class Methods

+ (ECAppDelegate *)sharedDelegate {
    return [[UIApplication sharedApplication] delegate];
}

#pragma mark - UIApplicationDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [Crashlytics startWithAPIKey:@"e1d89abfad5d73a039ef92ad3c192fbe7d4b60d4"];
    
    return YES;
}

@end
