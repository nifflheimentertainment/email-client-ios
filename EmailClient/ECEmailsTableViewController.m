//
//  ECInboxTableViewController.m
//  EmailClient
//
//  Created by Camilo Rodríguez Gaviria on 06/05/14.
//  Copyright (c) 2014 Nifflheim Entertainment. All rights reserved.
//

#import "ECEmailsTableViewController.h"
#import <SWTableViewCell/SWTableViewCell.h>

NSString *const ECEmailCellIdentifier = @"ECEmailCellIdentifier";

@interface ECEmailsTableViewController () <SWTableViewCellDelegate>

@property (strong, nonatomic) NSMutableArray *dataSource;

@end

@implementation ECEmailsTableViewController

#pragma mark - Initialization

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.title = [[self.folderName componentsSeparatedByString:@"/"] lastObject];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [SVProgressHUD showWithStatus:[NSString stringWithFormat:@"Fetching emails from %@", self.folderName]];
    
    MCOIMAPSession *session = [ECSessionManager sessionManager].session;
    MCOIMAPMessagesRequestKind requestKind = MCOIMAPMessagesRequestKindHeaders;
    MCOIndexSet *uids = [MCOIndexSet indexSetWithRange:MCORangeMake(1, UINT64_MAX)];
    MCOIMAPFetchMessagesOperation *operation = [session fetchMessagesByUIDOperationWithFolder:self.folderName requestKind:requestKind uids:uids];
    
    [operation start:^(NSError * error, NSArray * fetchedMessages, MCOIndexSet * vanishedMessages) {
        [SVProgressHUD dismiss];
        
        if(error) {
            [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
        } else {
            self.dataSource = [NSMutableArray arrayWithArray:fetchedMessages];
            
            [self.tableView reloadData];
        }
    }];
}

#pragma mark - Private

- (NSArray *)leftButtons
{
    NSMutableArray *leftUtilityButtons = [NSMutableArray new];
    
    [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor blueColor] title:@"Archive"];
    [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor redColor] title:@"Delete"];
    [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor greenColor] title:@"Short"];
    [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor orangeColor] title:@"Long"];
    [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor purpleColor] title:@"To Read"];
    
    return leftUtilityButtons;
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SWTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ECEmailCellIdentifier forIndexPath:indexPath];
    MCOIMAPMessage *message = [self.dataSource objectAtIndex:indexPath.row];
    
    cell.leftUtilityButtons = [self leftButtons];
    cell.textLabel.text = message.header.subject;
    cell.delegate = self;
    
    return cell;
}

#pragma mark - SWTableViewCellDelegate

- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state {
    BOOL swipeable = NO;
    
    if (state == kCellStateLeft) {
        swipeable = YES;
    }
    
    return swipeable;
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    MCOIMAPMessage *message = [self.dataSource objectAtIndex:indexPath.row];
    MCOIMAPSession *session = [ECSessionManager sessionManager].session;
    NSString *folder = @"";
    MCOIndexSet *indexSet = [MCOIndexSet indexSetWithIndex:message.uid];
    
    switch (index) {
        case 0:
            folder = @"ECFolder/Archive";
            break;
            
        case 1:
            folder = @"ECFolder/Delete";
            break;
            
        case 2:
            folder = @"ECFolder/Short Reply";
            break;
            
        case 3:
            folder = @"ECFolder/Long Reply";
            break;
            
        case 4:
            folder = @"ECFolder/To Read";
            break;
    }

    MCOIMAPCopyMessagesOperation *operation = [session copyMessagesOperationWithFolder:self.folderName uids:indexSet destFolder:folder];
    
    [operation start:^(NSError *error, NSDictionary *uidMapping) {
        if (error) {
            [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
        } else {
            NSString *labelName = [[folder componentsSeparatedByString:@"/"] lastObject];
            
            [SVProgressHUD showWithStatus:[NSString stringWithFormat:@"Email marked as %@", labelName]];
        }
    }];
}

@end
