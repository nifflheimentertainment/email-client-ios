//
//  ECAppDelegate.h
//  EmailClient
//
//  Created by Camilo Rodríguez Gaviria on 06/05/14.
//  Copyright (c) 2014 Nifflheim Entertainment. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ECAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
