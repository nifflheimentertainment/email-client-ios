//
//  main.m
//  EmailClient
//
//  Created by Camilo Rodríguez Gaviria on 06/05/14.
//  Copyright (c) 2014 Nifflheim Entertainment. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ECAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ECAppDelegate class]));
    }
}
