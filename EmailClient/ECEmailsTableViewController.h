//
//  ECInboxTableViewController.h
//  EmailClient
//
//  Created by Camilo Rodríguez Gaviria on 06/05/14.
//  Copyright (c) 2014 Nifflheim Entertainment. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ECEmailsTableViewController : UITableViewController

@property (assign, nonatomic) BOOL customFolder;
@property (strong, nonatomic) NSString *folderName;

@end
