//
//  ECSessionManager.h
//  EmailClient
//
//  Created by Camilo Rodríguez Gaviria on 21/06/14.
//  Copyright (c) 2014 Nifflheim Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ECSessionManager : NSObject

@property (strong, nonatomic) MCOIMAPSession *session;

+ (instancetype)sessionManager;
+ (void)updateUsername:(NSString *)username password:(NSString *)password;

- (NSString *)username;
- (NSString *)password;
- (void)updateUsername:(NSString *)username;
- (void)updatePassword:(NSString *)password;
- (void)clear;

@end
