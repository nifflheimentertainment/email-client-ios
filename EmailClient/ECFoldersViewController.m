//
//  ECFoldersViewController.m
//  EmailClient
//
//  Created by Camilo Rodríguez Gaviria on 19/06/14.
//  Copyright (c) 2014 Nifflheim Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ECFoldersViewController.h"
#import "ECEmailsTableViewController.h"

NSString *const ECFoldersViewControllerSegueLogOut = @"Logout";
NSString *const ECFoldersViewControllerSegueEmails = @"Emails";

NSString *const ECFolderCellIdentifier = @"FolderCellIdentifier";

@interface ECFoldersViewController () <UIAlertViewDelegate>

@property (strong, nonatomic) NSString *folderName;
@property (strong, nonatomic) MCOIMAPSession *session;
@property (strong, nonatomic) NSArray *dataSource;

@end

@implementation ECFoldersViewController

#pragma mark - Initialization

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    MCOIMAPSession *session = [ECSessionManager sessionManager].session;
    MCOIMAPFetchFoldersOperation *foldersOperation = [session fetchAllFoldersOperation];
    
    [SVProgressHUD showWithStatus:@"Fetching folders..." maskType:SVProgressHUDMaskTypeGradient];
    [foldersOperation start:^(NSError *error, NSArray *folders) {
        [SVProgressHUD dismiss];
        
        if (error) {
            [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
        } else {
            BOOL hasECFolders = NO;
            NSMutableArray *foldersToShow = [NSMutableArray arrayWithCapacity:0];
            
            for (MCOIMAPFolder *folder in folders) {
                if ([folder.path hasSuffix:@"ECFolder"] || [folder.path hasPrefix:@"ECFolder"]) {
                    hasECFolders = YES;
                } else if (![folder.path hasSuffix:@"[Gmail]"]) {
                    [foldersToShow addObject:folder];
                    [foldersToShow sortUsingComparator:^NSComparisonResult(MCOIMAPFolder *firstFolder, MCOIMAPFolder *secondFolder) {
                        NSString *delimiter = [NSString stringWithFormat:@"%c", folder.delimiter];
                        NSString *nameFirstFolder = [[firstFolder.path componentsSeparatedByString:delimiter] lastObject];
                        NSString *nameSecondFolder = [[secondFolder.path componentsSeparatedByString:delimiter] lastObject];
                        
                        return [nameFirstFolder localizedCaseInsensitiveCompare:nameSecondFolder];
                    }];
                }
            }
            
            self.dataSource = foldersToShow;
            
            if (!hasECFolders) {
                [[[UIAlertView alloc] initWithTitle:@"EC needs to create custom folders" message:@"EC will create new folders inside your Gmail account to keep things organized" delegate:self cancelButtonTitle:@"No, Do not create" otherButtonTitles:@"Yes, Create them", nil] show];
            }
            
            [self.tableView reloadData];
        }
    }];
    
    self.session = session;
}

#pragma mark - Private

- (void)createFolderWithName:(NSString *)name completionBlock:(void(^)(void))completion {
    __block MCOIMAPOperation *operation = [self.session createFolderOperation:name];
    [operation start:^(NSError *error) {
        if (error) {
            [[[UIAlertView alloc] initWithTitle:@"EC could not create folders" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        } else if (completion) {
            completion();
        }
    }];
}

#pragma mark - Overridden

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:ECFoldersViewControllerSegueEmails]) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:(UITableViewCell *)sender];
        MCOIMAPFolder *folder = [self.dataSource objectAtIndex:indexPath.row];
        ECEmailsTableViewController *emailsTableViewController = (ECEmailsTableViewController *)segue.destinationViewController;
        emailsTableViewController.folderName = folder.path;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ECFolderCellIdentifier forIndexPath:indexPath];
    MCOIMAPFolder *folder = [self.dataSource objectAtIndex:indexPath.row];
    NSArray *data = [folder.path componentsSeparatedByString:[NSString stringWithFormat:@"%c", folder.delimiter]];
    cell.textLabel.text = [data lastObject];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    self.folderName = cell.textLabel.text;
    
    if ([self.folderName isEqualToString:@"All Mail"]) {
        [UIAlertView showWithTitle:@"Warning" message:@"Fetching all mail could take a while, and it will use heavily your internet connection. Do you want to continue?" cancelButtonTitle:@"NO" otherButtonTitles:@[@"YES"] tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if (buttonIndex != alertView.cancelButtonIndex) {
                [self performSegueWithIdentifier:ECFoldersViewControllerSegueEmails sender:cell];
            }
        }];
    } else {
         [self performSegueWithIdentifier:ECFoldersViewControllerSegueEmails sender:cell];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex != alertView.cancelButtonIndex) {
        [self createFolderWithName:@"ECFolder" completionBlock:^{
            [self createFolderWithName:@"ECFolder/Archive" completionBlock:^{
                [self createFolderWithName:@"ECFolder/Delete" completionBlock:^{
                    [self createFolderWithName:@"ECFolder/Long Reply" completionBlock:^{
                        [self createFolderWithName:@"ECFolder/Short Reply" completionBlock:^{
                            [self createFolderWithName:@"ECFolder/To Read" completionBlock:nil];
                        }];
                    }];
                }];
            }];
        }];
    }
}

@end