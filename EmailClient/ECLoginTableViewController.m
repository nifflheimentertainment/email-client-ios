//
//  ECLoginTableViewController.m
//  EmailClient
//
//  Created by Camilo Rodríguez Gaviria on 20/06/14.
//  Copyright (c) 2014 Nifflheim Entertainment. All rights reserved.
//

#import "ECLoginTableViewController.h"

NSString *const ECLoginTableViewSegueMainMenu = @"MainMenu";

@interface ECLoginTableViewController () <UITextFieldDelegate>

@property (strong, nonatomic) ECSessionManager *sessionManager;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

- (IBAction)loginButtonTapped:(UIButton *)sender;
- (void)login;
- (BOOL)loginWithUsername:(NSString *)username password:(NSString *)password error:(NSError *__autoreleasing *)error;

@end

@implementation ECLoginTableViewController

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    
    ECSessionManager *sessionManager = [ECSessionManager sessionManager];
    NSString *username = [sessionManager username];
    NSString *password = [sessionManager password];
    NSError *error = nil;
    self.sessionManager = sessionManager;
    
    [self loginWithUsername:username password:password error:&error];
    
    if (error) {
        NSLog(@"No username/password stored. Starting from scratch");
    }
}

#pragma mark - Public

- (IBAction)unwindToLogin:(UIStoryboardSegue *)sender {
    
}

#pragma mark - Private

- (IBAction)loginButtonTapped:(UIButton *)sender {
    [self login];
}

- (void)login {
    NSString *username = self.usernameTextField.text;
    NSString *passworod = self.passwordTextField.text;
    NSError *error = nil;
    
    [self loginWithUsername:username password:passworod error:&error];
    
    if (error) {
        [[[UIAlertView alloc] initWithTitle:@"EC could not login" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}

- (BOOL)loginWithUsername:(NSString *)username password:(NSString *)password error:(NSError *__autoreleasing *)error {
    BOOL canLogin = NO;
    
    if ((!username && !password) || ([username length] == 0 && [password length] > 0)) {
        if (error) {
            *error = [NSError errorWithDomain:[[self class] description] code:0 userInfo:@{NSLocalizedDescriptionKey: @"Empty username and password"}];
        }
    } else if (!username || [username length] == 0) {
        if (error) {
            *error = [NSError errorWithDomain:[[self class] description] code:0 userInfo:@{NSLocalizedDescriptionKey: @"Empty username"}];
        }
    } else if (!password || [password length] == 0) {
        if (error) {
            *error = [NSError errorWithDomain:[[self class] description] code:0 userInfo:@{NSLocalizedDescriptionKey: @"Empty password"}];
        }
    } else {
        canLogin = YES;
        
        [ECSessionManager updateUsername:username password:password];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self performSegueWithIdentifier:ECLoginTableViewSegueMainMenu sender:nil];
        });
    }
    
    return canLogin;
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    if (textField == self.usernameTextField) {
        [self.passwordTextField becomeFirstResponder];
    } else if (textField == self.passwordTextField) {
        [self login];
    }
    
    return YES;
}

@end
